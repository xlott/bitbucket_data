import urllib
import re
import pymongo
import requests
import json




def fetch_pages(page, query):
        bit_url = 'https://bitbucket.org/repo/all/' +str(page) + '?name=' + query
        bit_html = urllib.request.urlopen(bit_url)
        bit_text = bit_html.read()
        find_text = bit_text.decode('utf-8')
        return find_text

def get_user(search):
    result_regex = 'Showing(.+?)results'
    page_num = 1
    page = fetch_pages(page_num,search)
    all = re.findall(re.compile(result_regex),page)
    Number = int(all[0])
    users_regex = re.compile('href=\"/(.+?)/')
    print(Number)
    user_set = set()
    while True:
        user_list = re.findall(users_regex, page)
        for user in user_list:
            if user not in user_set:
                yield user
                user_set.add(user)
        page_num += 1
        if page_num > int(Number/10)+1:
            break
        page = fetch_pages(page_num, search)

def user_add(search):
    mng_client = pymongo.MongoClient('localhost', 27017)
    mng_db = mng_client['Bitbucket']
    collection_name = 'profiles2.0_bitbucket'

   # if collection_name in mng_db.collection_names():
    #    mng_db.drop_collection(collection_name)

    col = mng_db[collection_name]

    for user in get_user(search):
        if col.find({'_id':user}).count()==0:
            url = 'https://api.bitbucket.org/2.0/users/' + user
            json_obj = requests.get(url)
            print(json_obj)
            if json_obj.status_code == 404:
                data = {}
                data['_id'] = user
                data['valid'] = False
                print('not valid ', user)
                col.insert_one(data)
            else :
                data = json_obj.json()
                data['_id'] = data['username']
                if data.get('links') and data['links'].get('repositories'):
                    data['repositories'] = requests.get(data['links']['repositories']['href']).json()
                print(data['username'])
                col.insert_one(data)


user_add('integration')
#user_add('crawler')
#user_add('python')
#user_add('java')
#user_add('C++')
