import urllib
import re
import urllib.request
import csv
import pymongo
import requests
import string
import itertools

def profiles_mongo(m,n):

    mng_client = pymongo.MongoClient('localhost', 27017)
    mng_db = mng_client['Bitbucket']
    collection_name = 'profiles_bit'

   # if collection_name in mng_db.collection_names():
    #    mng_db.drop_collection(collection_name)

    col = mng_db[collection_name]

    for i in  range(m,n):
        for profile in profiles(i):
            if col.find({'_id':profile}).count()==0:
                col.insert_one(profile)


def profiles(len):
    for key in keyword(len):
        url = 'https://api.bitbucket.org/1.0/users/' + key
        json_obj = requests.get(url)
        print(json_obj)
        if json_obj.status_code == 404:
            continue
        else :
            data = json_obj.json()
            data['_id'] = key
            print(data['user']['username'])
            yield data

def keyword(len):
    Allowed = string.ascii_lowercase + string.digits + '_-'
    for i in itertools.product(Allowed,repeat=len):
        yield ''.join(i)

profiles_mongo(3,4)
