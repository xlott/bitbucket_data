import urllib
import re
import urllib.request
import csv
import pymongo
import requests
import string
import itertools

def profiles_mongo(m,n):

    mng_client = pymongo.MongoClient('localhost', 27017)
    mng_db = mng_client['Bitbucket']
    collection_name = 'profiles2.0_bit'

    #if collection_name in mng_db.collection_names():
        #mng_db.drop_collection(collection_name)

    col = mng_db[collection_name]
    for i in  range(m,n):
        for profile in profiles(i,col):
            if col.find({'_id':profile['username']}).count()==0:
                col.insert_one(profile)


def profiles(len,col):
    for key in keyword(len):
        if col.find({'_id':re.compile(key, re.IGNORECASE)}).count()==0:
            print(key)
            url = 'https://api.bitbucket.org/2.0/users/' + key
            json_obj = requests.get(url)
            print(json_obj)
            if json_obj.status_code == 404:
                continue
            else :
                data = json_obj.json()
                if data.get('error'):
                    continue
            data['_id'] = data['username']
            if data.get('links') and data['links'].get('repositories'):
                data['repositories'] = requests.get(data['links']['repositories']['href']).json()
            print(data['username'])
            yield data

def keyword(len):
    Allowed = string.ascii_lowercase + string.digits + '_-'
    for i in itertools.product(Allowed,repeat=len):
        yield ''.join(i)

profiles_mongo(1,2)
